// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:leak_detector/leak_detector.dart';
import 'package:memory_leak/controller_leak.dart';
import 'package:memory_leak/images.dart';

import 'package:memory_leak/main.dart';

void main() {
  int currentPage = 0;
  bool hasLeak = false;
  GlobalKey<NavigatorState> navigatorKey = GlobalKey();
  LeakDetector().init(maxRetainingPath: 300);
  StreamController<String> _leakController = StreamController.broadcast();
  LeakDetector().onLeakedStream.listen((LeakedInfo info) {
    //print to console
    info.retainingPath.forEach((node) => print(node));
    //show preview page
    print('has leak');

    _leakController.add(info.retainingPath.first.clazz);
  });
  List<Widget> appPages = [
    ControllerLeak(),
    IncrementLeak(),
  ];
  testWidgets('Memory Leak test', (WidgetTester tester) async {
    dynamic returendValue;

    var pushButton = ElevatedButton(
        child: Text('press on me'),
        key: Key('push'),
        onPressed: (() async {
          returendValue = Navigator.of(navigatorKey.currentContext!)
              .pushNamed(currentPage.toString());
        }));

    await tester.runAsync(() async {
      await tester.pumpWidget(MaterialApp(
        navigatorKey: navigatorKey,
        routes: {
          '/': (context) {
            return Column(
              children: [
                Container(
                  child: pushButton,
                ),
              ],
            );
          },
          ...Map.fromIterable(
            appPages,
            key: (element) => '${currentPage++}',
            value: (element) => (context) => element,
          )
        },
        navigatorObservers: [
          LeakNavigatorObserver(
            shouldCheck: (route) {
              //You can customize which `route` can be detected
              return true; //route.settings.name != null && route.settings.name != '/';
            },
          )
        ],
      ));
      await tester.pumpAndSettle();
      currentPage = 0;
      String currntPage = '';
      var subscription = _leakController.stream.listen((event) {
        expect(event, '', reason: '$event was leaking');
      });
      for (var page in appPages) {
        print('current page $page');
        await tester.tap(find.byWidget(pushButton));
        await tester.pumpAndSettle();
        Navigator.of(navigatorKey.currentContext!).pop();
        print(returendValue);
        await tester.pumpAndSettle();
        await Future.delayed(Duration(seconds: 6));
      }
      subscription.cancel();
    });
  });
}
