import 'dart:async';

import 'package:flutter/material.dart';
import 'package:leak_detector/leak_detector.dart';
import 'package:memory_leak/images.dart';

import 'controller_leak.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

GlobalKey<NavigatorState> navigatorKey = GlobalKey();

class _MyAppState extends State<MyApp> {
  bool _checking = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // LeakDetector().init(maxRetainingPath: 300);
    // LeakDetector().onLeakedStream.listen((LeakedInfo info) {
    //   //print to console
    //   info.retainingPath.forEach((node) => print(node));
    //   //show preview page
    //   showLeakedInfoPage(navigatorKey.currentContext!, info);
    // });
    // LeakDetector().onEventStream.listen((DetectorEvent event) {
    //   print(event);
    //   if (event.type == DetectorEventType.startAnalyze) {
    //     setState(() {
    //       _checking = true;
    //     });
    //   } else if (event.type == DetectorEventType.endAnalyze) {
    //     setState(() {
    //       _checking = false;
    //     });
    //   }
    // });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      navigatorObservers: [
        //used the LeakNavigatorObserver.
        // LeakNavigatorObserver(
        //   shouldCheck: (route) {
        //     //You can customize which `route` can be detected
        //     return route.settings.name != null && route.settings.name != '/';
        //   },
        // ),
      ],
      routes: {
        '/': (context) => MyHomePage(
              title: 'memoryLeak',
            ),
        '/ControllerLeak': (context) => ControllerLeak(),
        '/IncrementLeak': (context) => IncrementLeak(),
        '/ImagesLeak': (context) => ImagesLeak(),
      },
      //home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class IncrementLeak extends StatefulWidget {
  IncrementLeak({Key? key}) : super(key: key);

  @override
  State<IncrementLeak> createState() => _IncrementLeakState();
}

class _IncrementLeakState extends State<IncrementLeak> {
  int _counter = 0;

  late final MyIncrementer _incrementer = MyIncrementer((count) => setState(() {
        if (mounted) {
          _counter++;
        }
      }));

  void _increment(BuildContext context) {
    // Timer.periodic(Duration(seconds: 20), (timer) async {
    //do somthing in the future
    // await Future.delayed(Duration(seconds: 10));
    // if (mounted) {
    _incrementer.increment(_counter);
    _incrementer.count = _counter;
    // }
    //_incrementer.increment();
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop(_incrementer);
            }),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _increment(context),
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class MyIncrementer {
  MyIncrementer(this.increment);
  int _count = 0;
  set count(int value) {
    _count = value;
  }

  double calculate() {
    return _count * 2.0;
  }

  final Function(int) increment;
}

class _MyHomePageState extends State<MyHomePage> {
  // void _updateAndInvokeIncrementer(BuildContext context) {
  //   final incrementer = _incrementer;

  //   _incrementer = MyIncrementer(() {
  //     setState(() {
  //       _counter++;
  //     });
  //   });

  //   _incrementer.increment();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            IconButton(
                onPressed: () async {
                  var incrementer =
                      (await Navigator.pushNamed(context, '/IncrementLeak'))
                          as MyIncrementer?;
                  print(incrementer?.calculate());
                },
                icon: Icon(Icons.add_circle_outline_rounded)),
            IconButton(
                onPressed: () {
                  Navigator.of(navigatorKey.currentContext!).pushNamed(
                      //controllerLeak )
                      '/ControllerLeak');
                },
                icon: Icon(Icons.control_point_duplicate_rounded)),
            IconButton(
                onPressed: () {
                  Navigator.of(navigatorKey.currentContext!).pushNamed(
                      //controllerLeak )
                      '/ImagesLeak');
                },
                icon: Icon(Icons.image_rounded)),
          ],
        ),
      ),
    );
  }
}
