import 'dart:async';

import 'package:flutter/material.dart';

import 'dead_object_detector.dart';

class ControllerLeak extends StatefulWidget {
  ControllerLeak({Key? key}) : super(key: key);

  @override
  State<ControllerLeak> createState() => _ControllerLeakState();
}

class _ControllerLeakState extends State<ControllerLeak> with LeakStateMixin {
  late final StreamSubscription _subscription;
  @override
  void initState() {
    _subscription = Stream.periodic(Duration(seconds: 1)).listen((event) {
      print('event: $event   in ${DateTime.now()}  ');
    });
    super.initState();
  }

  @override
  void dispose() {
    print('disposing');
    // _subscription.cancel();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(child: Container(child: Text('ControllerLeak'))));
  }
}
