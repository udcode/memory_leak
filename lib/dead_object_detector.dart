import 'package:flutter/widgets.dart';

class DeadObjectWatcher {
  static DeadObjectWatcher instance = DeadObjectWatcher._();

  DeadObjectWatcher._();
  final Expando<Object> weakRef = Expando('weakreferences');

  void watch(Object obj) {
    instance.weakRef[obj] = obj.hashCode;
  }
}

mixin LeakStateMixin<T extends StatefulWidget> on State<T> {
  @override
  void dispose() {
    DeadObjectWatcher.instance
      ..watch(context)
      ..watch(this);
    super.dispose();
  }
}
